#ifndef _BOUNCERBITMAP_H
#define _BOUNCERBITMAP_H
#include "Bouncer.h"

class BouncerBitMap : public Bouncer{
	
	float position_x;
	float position_y;
	ALLEGRO_BITMAP* grafico;
	
	public :
		BouncerBitMap(float width, float height, float positionX, float positionY)
			: Bouncer(width, height)    // Call the superclass constructor in the subclass' initialization list.
		{
			position_x = positionX;
			position_y = positionY;			
			grafico = al_create_bitmap(width, height);
		}
		
		ALLEGRO_BITMAP* GetBitMap(){
			return grafico;
		}


		float GetWidth()
		{
			return width;
		}

		float GetHeight()
		{
			return height;
		}

		float GetPositionX()
		{
			return position_x;
		}

		float GetPositionY()
		{
			return position_y;
		}
   
};

#endif