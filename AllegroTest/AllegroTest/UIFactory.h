#ifndef _UIFACTORY_H
#define _UIFACTORY_H
#include "BouncerBitMap.h"
/*
* Clase que se encarga de crear todos los bitmaps del juego
* podria asociarse con otra clase que sea la que renderice los
* niveles, etc.
*/
class UIFactory{
	BouncerBitMap* bouncer;

	public:
		float FPS;
		static const int SCREEN_W = 600; //ancho pantalla
		static const int SCREEN_H = 400; //alto pantalla
		
		UIFactory(){ FPS = 30.0; }
		

		bool CreateInitialBitMaps(){
			//TODO: Definir excepciones, constantes de tama�os
			//try{
				bouncer = new BouncerBitMap(32.0, 32.0, (SCREEN_W / 2.0) , (SCREEN_H /2.0));
				
				return true;
			/*}catch(int e){
				
			};			*/
			
		}

		BouncerBitMap* GetBouncer(){
			return bouncer;
		}

};
#endif