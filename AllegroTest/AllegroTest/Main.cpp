#include <stdio.h>
#include <allegro5/allegro.h>
#include <BouncerBitMap.h>
#include <UIFactory.h>

 
int main(int argc, char **argv){
   
   ALLEGRO_DISPLAY *display = NULL;
   ALLEGRO_EVENT_QUEUE *event_queue = NULL;
   ALLEGRO_TIMER *timer = NULL;
   ALLEGRO_BITMAP *bouncer = NULL;
   UIFactory *ui = new UIFactory();
   
   
   bool redraw = true;
 
   if(!al_init()) {
      fprintf(stderr, "failed to initialize allegro!\n");
      return -1;
   }
 
    if(!al_install_keyboard()) {
      fprintf(stderr, "failed to initialize the keyboard!\n");
      return -1;
   }

	timer = al_create_timer(1.0 / ui->FPS);
   if(!timer) {
      fprintf(stderr, "failed to create timer!\n");
      return -1;
   }
 
   display = al_create_display(ui->SCREEN_W, ui->SCREEN_H);
   if(!display) {
      fprintf(stderr, "failed to create display!\n");
      al_destroy_timer(timer);
      return -1;
   }
   
   ui->CreateInitialBitMaps();

   bouncer = ui->GetBouncer()->GetBitMap();

   if(!bouncer) {
      fprintf(stderr, "failed to create bouncer bitmap!\n");
      al_destroy_display(display);
      al_destroy_timer(timer);
      return -1;
   }
 
   al_set_target_bitmap(bouncer);
 
   al_clear_to_color(al_map_rgb(255, 0, 255));
 
   al_set_target_bitmap(al_get_backbuffer(display));
 
   event_queue = al_create_event_queue();
   if(!event_queue) {
      fprintf(stderr, "failed to create event_queue!\n");
      al_destroy_bitmap(bouncer);
      al_destroy_display(display);
      al_destroy_timer(timer);
      return -1;
   }
 
   al_register_event_source(event_queue, al_get_display_event_source(display));
 
   al_register_event_source(event_queue, al_get_timer_event_source(timer));

    al_register_event_source(event_queue, al_get_keyboard_event_source());
 
   al_clear_to_color(al_map_rgb(0,0,0));
 
   al_flip_display();
 
   al_start_timer(timer);
	
   float bouncer_x = ui->GetBouncer()->GetPositionX();
   float bouncer_y = ui->GetBouncer()->GetPositionY();

   while(1)
   {
      ALLEGRO_EVENT ev;
     al_wait_for_event(event_queue, &ev);
     redraw = true;
      if(ev.type == ALLEGRO_EVENT_TIMER) {
/*
         if(bouncer_x < 0 || bouncer_x > SCREEN_W - BOUNCER_SIZE) {
            bouncer_dx = -bouncer_dx;
         }
 
         if(bouncer_y < 0 || bouncer_y > SCREEN_H - BOUNCER_SIZE) {
            bouncer_dy = -bouncer_dy;
         }
 
         bouncer_x += bouncer_dx;
         bouncer_y += bouncer_dy;
 
         redraw = true;*/
      }
      else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
         break;
	  }else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
		 
		  if(ev.keyboard.keycode == ALLEGRO_KEY_LEFT){
			

							 bouncer_x = bouncer_x - 10;
							 //bouncer_y += bouncer_dy;
 
							 //redraw = true;
							  al_clear_to_color(al_map_rgb(0,0,0));
 
							 al_draw_bitmap(bouncer, bouncer_x, bouncer_y, 0);
 
							 al_flip_display();
							 
							 al_rest(1.0);

							 if(ev.type == ALLEGRO_EVENT_KEY_UP)
							 {
								 break;
							 }
				 			// al_draw_bitmap(bouncer, bouncer_x, bouncer_y, 0);

			 

		  }else if(ev.keyboard.keycode == ALLEGRO_KEY_RIGHT){
			 
			 
			 bouncer_x = bouncer_x + 10;
			 //bouncer_y += bouncer_dy;
 
			 redraw = true;

		  }


      }
 
      if(redraw && al_is_event_queue_empty(event_queue)) {
         redraw = false;
 
         al_clear_to_color(al_map_rgb(0,0,0));
 
         al_draw_bitmap(bouncer, bouncer_x, bouncer_y, 0);
 
         al_flip_display();
      }
   }
 
   al_destroy_bitmap(bouncer);
   al_destroy_timer(timer);
   al_destroy_display(display);
   al_destroy_event_queue(event_queue);
 
   return 0;
}